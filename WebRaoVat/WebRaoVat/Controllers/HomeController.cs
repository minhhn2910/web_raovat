﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebRaoVat.Models;

namespace WebRaoVat.Controllers
{
    public class HomeController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();
        public ActionResult Index(int CatId = 0, int page = 1, int PageSize = 10, int SortBy = 1, bool IsAsc = true, string Search = null)
        {
            if (CatId != 0) // user want to view specific category's post
                return RedirectToAction("PostCategory", "Details", new { CatId = CatId });

            /*  var posts = db.Posts.Include(p => p.PostCategory).Include(p => p.UserProfile).Include(p => p.Zone);*/
            //tim kiem post
            var posts = db.Posts.Where(
                                    p => Search == null
                                    || p.Title.Contains(Search)
                                    || p.ShortDescription.Contains(Search)
                                    || p.Detail.Contains(Search)
                                    || p.TitleAlias.Contains(Search));
            posts = posts.Where(t => t.PostType == 1);
            IEnumerable<Post> postlist;
            switch (SortBy)
            {
                case 1:
                    postlist = IsAsc ? posts.OrderBy(p => p.Title) : posts.OrderByDescending(p => p.Title);
                    break;
                case 2:
                    postlist = IsAsc ? posts.OrderBy(p => p.Created) : posts.OrderByDescending(p => p.Created);
                    break;
                case 3:
                    postlist = IsAsc ? posts.OrderBy(p => p.HitCount) : posts.OrderByDescending(p => p.HitCount);
                    break;
                case 4:
                    postlist = IsAsc ? posts.OrderBy(p => p.Price) : posts.OrderByDescending(p => p.Price);
                    break;
                default:
                    postlist = IsAsc ? posts.OrderBy(p => p.PostId) : posts.OrderByDescending(p => p.PostId);
                    break;
            }
            ViewBag.CurrentPage = page;
            ViewBag.PageSize = PageSize;
            ViewBag.TotalPages = (int)Math.Ceiling((double)postlist.Count() / PageSize);
            ViewBag.SortBy = SortBy;
            ViewBag.IsAsc = IsAsc;
            ViewBag.Search = Search;
            postlist = postlist.Skip((page - 1) * PageSize).Take(PageSize).ToList();
            
            ViewBag.Message = "Trang chủ";

            return View(postlist.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

         public ActionResult HuongDanThanhToan()
         {
             return View();
         }
         public ActionResult QuyDinhDoiTra()
         {
             return View();
         }
         public ActionResult LienHeHopTac()
         {
             return View();
         }

         public ActionResult QuyDinhDangTin()
         {
             return View();
         }
         public ActionResult DieuKhoan()
         {
             return View();
         }
         public ActionResult ChinhSach()
         {
             return View();
         }
         public ActionResult KinhNghiemDangTin()
         {
             return View();
         }
         public ActionResult GioiThieu()
         {
             return View();
         }
         public ActionResult MucTieu()
         {
             return View();
         }
         public ActionResult TuyenDung()
         {
             return View();
         }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
