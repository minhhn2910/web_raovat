﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using WebRaoVat.Filters;
using WebRaoVat.Models;

namespace WebRaoVat.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class PostController : Controller
    {
        private raovat_dbEntities db = new raovat_dbEntities();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index(int CatId = 0,int page = 1, int PageSize = 5, int SortBy = 1, bool IsAsc = true, string Search = null)
        {
            if (CatId != 0) // user want to view specific category's post
                return RedirectToAction("PostCategory", "Details", new { CatId = CatId });

          /*  var posts = db.Posts.Include(p => p.PostCategory).Include(p => p.UserProfile).Include(p => p.Zone);*/
            //tim kiem post
            var posts = db.Posts.Where(
                                    p => Search == null
                                    || p.Title.Contains(Search)
                                    || p.ShortDescription.Contains(Search)
                                    || p.Detail.Contains(Search)
                                    || p.TitleAlias.Contains(Search));
            IEnumerable<Post> postlist;
            switch (SortBy)
            {
                case 1:
                    postlist = IsAsc ? posts.OrderBy(p => p.Title) : posts.OrderByDescending(p => p.Title);
                    break;
                case 2:
                    postlist = IsAsc ? posts.OrderBy(p => p.Created) : posts.OrderByDescending(p => p.Created);
                    break;
                case 3:
                    postlist = IsAsc ? posts.OrderBy(p => p.HitCount) : posts.OrderByDescending(p => p.HitCount);
                    break;
                case 4:
                    postlist = IsAsc ? posts.OrderBy(p => p.Price) : posts.OrderByDescending(p => p.Price);
                    break;
                default:
                    postlist = IsAsc ? posts.OrderBy(p => p.PostId) : posts.OrderByDescending(p => p.PostId);
                    break;
            }
            ViewBag.CurrentPage = page;
            ViewBag.PageSize = PageSize;
            ViewBag.TotalPages = (int)Math.Ceiling((double)postlist.Count() / PageSize);
            ViewBag.SortBy = SortBy;
            ViewBag.IsAsc = IsAsc;
            ViewBag.Search = Search;
            postlist = postlist.Skip((page - 1) * PageSize).Take(PageSize).ToList();
            return View(postlist.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            
            Post post = db.Posts.Find(id);
            
            if (post == null)
            {
                return HttpNotFound();
            }
            if ((WebSecurity.CurrentUserId == post.UserId )|| (WebSecurity.CurrentUserName =="admin"))
                ViewBag.Editable = 1;
            else
                ViewBag.Editable = 0;
            db.Entry(post).State = EntityState.Modified;
            post.HitCount = ((post.HitCount != null) ? post.HitCount + 1 : 1);
            db.SaveChanges();
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName");
            ViewBag.UserId = WebSecurity.CurrentUserId;
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName");
            ViewBag.ShortDescription = "";
            ViewBag.Detail = "";
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post)
        {
          string[] images_links=null;
          if (Request["images"] != null)
             images_links = Request["images"].ToString().Split(':');

           if (ModelState.IsValid)
                {
                    //first link is thumbnail;
                    if (!String.IsNullOrEmpty(images_links[0]))
                        post.Thumbnail = images_links[0];
                    post.TitleAlias = "sample-alias"; //processing alias here
                    post.Created = System.DateTime.Now;
                    post.Active = true;
                    post.HitCount = 1;
                    if (WebSecurity.CurrentUserName != "admin" && post.CategoryId == 14)
                        post.CategoryId = 12;
                    db.Posts.Add(post);

                            for (int i = 1; i < images_links.Length; i++) //skip first image
                            {
                                if (!String.IsNullOrEmpty(images_links[i]))
                                {
                                    PostImage post_image;
                                    post_image = new PostImage();
                                    post_image.ImageURL = images_links[i].ToString();
                                    post_image.PostId = post.PostId;
                                    db.PostImages.Add(post_image);
                                }
                            }
                    
                        db.SaveChanges();
                    return RedirectToAction("Details", new { id = post.PostId });
           }
           ViewBag.ShortDescription = post.ShortDescription;
           ViewBag.Detail = post.Detail;
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", post.CategoryId);
            ViewBag.UserId = ViewBag.UserId = WebSecurity.CurrentUserId;
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName", post.ZoneId);
            if (images_links.Length>0 && images_links[0]!="")
             ViewBag.Images = images_links;
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null || (WebSecurity.CurrentUserId!=post.UserId && WebSecurity.CurrentUserName!="admin")) //only user authorized can edit
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", post.CategoryId);
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName", post.ZoneId);
            List<string> ImagesCollection = new List<string>();
            ImagesCollection.Add(post.Thumbnail);
            ImagesCollection.AddRange(post.PostImages.Select(a => a.ImageURL).ToList());
            ViewBag.Images = ImagesCollection.ToArray();
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {       
            string[] images_links = null;
            if (Request["images"] != null)
                images_links = Request["images"].ToString().Split(':');

            if(post.UserId!=WebSecurity.CurrentUserId)
                return HttpNotFound();
//            if (ModelState.IsValid)
//            {
                //first link is thumbnail;
                if (!String.IsNullOrEmpty(images_links[0]))
                    post.Thumbnail = images_links[0];
                post.TitleAlias = "sample-alias"; //processing alias here
                if (WebSecurity.CurrentUserName != "admin" && post.CategoryId == 14)
                    post.CategoryId = 12;
                db.Entry(post).State = EntityState.Modified;

                for (int i = 1; i < images_links.Length; i++) //skip first image
                {
                    if (!String.IsNullOrEmpty(images_links[i]))
                    {
                        PostImage post_image;
                        post_image = new PostImage();
                        post_image.ImageURL = images_links[i].ToString();
                        post_image.PostId = post.PostId;
                        db.PostImages.Add(post_image);
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Details", new { id = post.PostId });
    //        }
/*
            ViewBag.CategoryId = new SelectList(db.PostCategories, "CategoryId", "CategoryName", post.CategoryId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserId);
            ViewBag.ZoneId = new SelectList(db.Zones, "ZoneId", "ZoneName", post.ZoneId);

            if (images_links != null)
                ViewBag.Images = images_links;
            
            return View(post);
 * */
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
/*            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);*/ 
            // not necessary to confirm, only user who created post or admin/mod can delete
            //first step, only user created post can delete
            if ((WebSecurity.CurrentUserId == post.UserId) ||(WebSecurity.CurrentUserName == "admin" ))
            {
     /*           IEnumerable<PostImage> post_images = db.PostImages.Where(t => t.PostId == post.PostId);
                foreach (PostImage image in post_images)
                {
                    db.PostImages.Remove(image);
                    db.SaveChanges();
                }*/
                db.Posts.Remove(post);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}