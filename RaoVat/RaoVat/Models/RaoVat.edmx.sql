
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 12/12/2014 10:48:28
-- Generated from EDMX file: D:\web_raovat\RaoVat\RaoVat\Models\RaoVat.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [raovat_data];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[News]', 'U') IS NOT NULL
    DROP TABLE [dbo].[News];
GO
IF OBJECT_ID(N'[dbo].[NewsColumns]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NewsColumns];
GO
IF OBJECT_ID(N'[dbo].[NewsTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NewsTypes];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[UserTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserTypes];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'News'
CREATE TABLE [dbo].[News] (
    [NewsID] int IDENTITY(1,1) NOT NULL,
    [NewsColumnID] int  NOT NULL,
    [NewsZone] nvarchar(max)  NULL,
    [NewsContent] nvarchar(max)  NOT NULL,
    [Image] nvarchar(max)  NOT NULL,
    [PersonInfo] nvarchar(max)  NOT NULL,
    [Policy] nvarchar(max)  NOT NULL,
    [LocationMap] nvarchar(max)  NOT NULL,
    [SecurityCode] nvarchar(max)  NOT NULL,
    [NewsTypeID] int  NOT NULL
);
GO

-- Creating table 'NewsColumns'
CREATE TABLE [dbo].[NewsColumns] (
    [ColumnID] int IDENTITY(1,1) NOT NULL,
    [ColumnName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'NewsTypes'
CREATE TABLE [dbo].[NewsTypes] (
    [TypeID] int IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [Ewallet] nvarchar(max)  NOT NULL,
    [UserTypeID] nvarchar(max)  NOT NULL,
    [UserName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UserTypes'
CREATE TABLE [dbo].[UserTypes] (
    [UserTypeID] int IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [NewsID] in table 'News'
ALTER TABLE [dbo].[News]
ADD CONSTRAINT [PK_News]
    PRIMARY KEY CLUSTERED ([NewsID] ASC);
GO

-- Creating primary key on [ColumnID] in table 'NewsColumns'
ALTER TABLE [dbo].[NewsColumns]
ADD CONSTRAINT [PK_NewsColumns]
    PRIMARY KEY CLUSTERED ([ColumnID] ASC);
GO

-- Creating primary key on [TypeID] in table 'NewsTypes'
ALTER TABLE [dbo].[NewsTypes]
ADD CONSTRAINT [PK_NewsTypes]
    PRIMARY KEY CLUSTERED ([TypeID] ASC);
GO

-- Creating primary key on [UserID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [UserTypeID] in table 'UserTypes'
ALTER TABLE [dbo].[UserTypes]
ADD CONSTRAINT [PK_UserTypes]
    PRIMARY KEY CLUSTERED ([UserTypeID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------