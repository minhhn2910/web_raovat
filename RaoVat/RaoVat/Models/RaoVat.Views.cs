//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: System.Data.Mapping.EntityViewGenerationAttribute(typeof(Edm_EntityMappingGeneratedViews.ViewsForBaseEntitySetsAB8D5760986172A57C915EF8F498A6BE3BC77815FBA103BF35C63A0F7A5E4F1F))]

namespace Edm_EntityMappingGeneratedViews
{
    
    
    /// <Summary>
    /// The type contains views for EntitySets and AssociationSets that were generated at design time.
    /// </Summary>
    public sealed class ViewsForBaseEntitySetsAB8D5760986172A57C915EF8F498A6BE3BC77815FBA103BF35C63A0F7A5E4F1F : System.Data.Mapping.EntityViewContainer
    {
        
        /// <Summary>
        /// The constructor stores the views for the extents and also the hash values generated based on the metadata and mapping closure and views.
        /// </Summary>
        public ViewsForBaseEntitySetsAB8D5760986172A57C915EF8F498A6BE3BC77815FBA103BF35C63A0F7A5E4F1F()
        {
            this.EdmEntityContainerName = "RaoVatContainer1";
            this.StoreEntityContainerName = "RaoVatStoreContainer";
            this.HashOverMappingClosure = "f3f7e2eb60a9cea0309fcf82a3bced6e4777b35b84deb4a638ded5837d1bb6a9";
            this.HashOverAllExtentViews = "5e3d0cc446b3ceb654e5d19788836adb0eb710c460d085616200dc9f370d6c6d";
            this.ViewCount = 10;
        }
        
        /// <Summary>
        /// The method returns the view for the index given.
        /// </Summary>
        protected override System.Collections.Generic.KeyValuePair<string, string> GetViewAt(int index)
        {
            if ((index == 0))
            {
                return GetView0();
            }
            if ((index == 1))
            {
                return GetView1();
            }
            if ((index == 2))
            {
                return GetView2();
            }
            if ((index == 3))
            {
                return GetView3();
            }
            if ((index == 4))
            {
                return GetView4();
            }
            if ((index == 5))
            {
                return GetView5();
            }
            if ((index == 6))
            {
                return GetView6();
            }
            if ((index == 7))
            {
                return GetView7();
            }
            if ((index == 8))
            {
                return GetView8();
            }
            if ((index == 9))
            {
                return GetView9();
            }
            throw new System.IndexOutOfRangeException();
        }
        
        /// <Summary>
        /// return view for RaoVatStoreContainer.News
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView0()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatStoreContainer.News", @"
    SELECT VALUE -- Constructing News
        [RaoVat.Store.News](T1.News_NewsID, T1.News_NewsColumnID, T1.News_NewsZone, T1.News_NewsContent, T1.News_Image, T1.News_PersonInfo, T1.News_Policy, T1.News_LocationMap, T1.News_SecurityCode, T1.News_NewsTypeID)
    FROM (
        SELECT 
            T.NewsID AS News_NewsID, 
            T.NewsColumnID AS News_NewsColumnID, 
            T.NewsZone AS News_NewsZone, 
            T.NewsContent AS News_NewsContent, 
            T.Image AS News_Image, 
            T.PersonInfo AS News_PersonInfo, 
            T.Policy AS News_Policy, 
            T.LocationMap AS News_LocationMap, 
            T.SecurityCode AS News_SecurityCode, 
            T.NewsTypeID AS News_NewsTypeID, 
            True AS _from0
        FROM RaoVatContainer1.News AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatContainer1.News
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView1()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatContainer1.News", @"
    SELECT VALUE -- Constructing News
        [RaoVat.News](T1.News_NewsID, T1.News_NewsColumnID, T1.News_NewsZone, T1.News_NewsContent, T1.News_Image, T1.News_PersonInfo, T1.News_Policy, T1.News_LocationMap, T1.News_SecurityCode, T1.News_NewsTypeID)
    FROM (
        SELECT 
            T.NewsID AS News_NewsID, 
            T.NewsColumnID AS News_NewsColumnID, 
            T.NewsZone AS News_NewsZone, 
            T.NewsContent AS News_NewsContent, 
            T.Image AS News_Image, 
            T.PersonInfo AS News_PersonInfo, 
            T.Policy AS News_Policy, 
            T.LocationMap AS News_LocationMap, 
            T.SecurityCode AS News_SecurityCode, 
            T.NewsTypeID AS News_NewsTypeID, 
            True AS _from0
        FROM RaoVatStoreContainer.News AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatStoreContainer.NewsColumns
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView2()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatStoreContainer.NewsColumns", @"
    SELECT VALUE -- Constructing NewsColumns
        [RaoVat.Store.NewsColumns](T1.NewsColumns_ColumnID, T1.NewsColumns_ColumnName)
    FROM (
        SELECT 
            T.ColumnID AS NewsColumns_ColumnID, 
            T.ColumnName AS NewsColumns_ColumnName, 
            True AS _from0
        FROM RaoVatContainer1.NewsColumns AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatContainer1.NewsColumns
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView3()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatContainer1.NewsColumns", @"
    SELECT VALUE -- Constructing NewsColumns
        [RaoVat.NewsColumn](T1.NewsColumn_ColumnID, T1.NewsColumn_ColumnName)
    FROM (
        SELECT 
            T.ColumnID AS NewsColumn_ColumnID, 
            T.ColumnName AS NewsColumn_ColumnName, 
            True AS _from0
        FROM RaoVatStoreContainer.NewsColumns AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatStoreContainer.NewsTypes
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView4()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatStoreContainer.NewsTypes", @"
    SELECT VALUE -- Constructing NewsTypes
        [RaoVat.Store.NewsTypes](T1.NewsTypes_TypeID, T1.NewsTypes_TypeName)
    FROM (
        SELECT 
            T.TypeID AS NewsTypes_TypeID, 
            T.TypeName AS NewsTypes_TypeName, 
            True AS _from0
        FROM RaoVatContainer1.NewsTypes AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatContainer1.NewsTypes
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView5()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatContainer1.NewsTypes", @"
    SELECT VALUE -- Constructing NewsTypes
        [RaoVat.NewsType](T1.NewsType_TypeID, T1.NewsType_TypeName)
    FROM (
        SELECT 
            T.TypeID AS NewsType_TypeID, 
            T.TypeName AS NewsType_TypeName, 
            True AS _from0
        FROM RaoVatStoreContainer.NewsTypes AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatStoreContainer.Users
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView6()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatStoreContainer.Users", @"
    SELECT VALUE -- Constructing Users
        [RaoVat.Store.Users](T1.Users_UserID, T1.Users_Name, T1.Users_Email, T1.Users_Phone, T1.Users_Address, T1.Users_Ewallet, T1.Users_UserTypeID)
    FROM (
        SELECT 
            T.UserID AS Users_UserID, 
            T.Name AS Users_Name, 
            T.Email AS Users_Email, 
            T.Phone AS Users_Phone, 
            T.Address AS Users_Address, 
            T.Ewallet AS Users_Ewallet, 
            T.UserTypeID AS Users_UserTypeID, 
            True AS _from0
        FROM RaoVatContainer1.Users AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatContainer1.Users
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView7()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatContainer1.Users", @"
    SELECT VALUE -- Constructing Users
        [RaoVat.User](T1.User_UserID, T1.User_Name, T1.User_Email, T1.User_Phone, T1.User_Address, T1.User_Ewallet, T1.User_UserTypeID)
    FROM (
        SELECT 
            T.UserID AS User_UserID, 
            T.Name AS User_Name, 
            T.Email AS User_Email, 
            T.Phone AS User_Phone, 
            T.Address AS User_Address, 
            T.Ewallet AS User_Ewallet, 
            T.UserTypeID AS User_UserTypeID, 
            True AS _from0
        FROM RaoVatStoreContainer.Users AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatStoreContainer.UserTypes
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView8()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatStoreContainer.UserTypes", @"
    SELECT VALUE -- Constructing UserTypes
        [RaoVat.Store.UserTypes](T1.UserTypes_UserTypeID, T1.UserTypes_TypeName, T1.UserTypes_Description)
    FROM (
        SELECT 
            T.UserTypeID AS UserTypes_UserTypeID, 
            T.TypeName AS UserTypes_TypeName, 
            T.Description AS UserTypes_Description, 
            True AS _from0
        FROM RaoVatContainer1.UserTypes AS T
    ) AS T1");
        }
        
        /// <Summary>
        /// return view for RaoVatContainer1.UserTypes
        /// </Summary>
        private System.Collections.Generic.KeyValuePair<string, string> GetView9()
        {
            return new System.Collections.Generic.KeyValuePair<string, string>("RaoVatContainer1.UserTypes", @"
    SELECT VALUE -- Constructing UserTypes
        [RaoVat.UserType](T1.UserType_UserTypeID, T1.UserType_TypeName, T1.UserType_Description)
    FROM (
        SELECT 
            T.UserTypeID AS UserType_UserTypeID, 
            T.TypeName AS UserType_TypeName, 
            T.Description AS UserType_Description, 
            True AS _from0
        FROM RaoVatStoreContainer.UserTypes AS T
    ) AS T1");
        }
    }
}
