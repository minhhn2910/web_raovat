﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RaoVat.Models;

namespace RaoVat.Controllers
{
    public class NewsColumnController : Controller
    {
        private RaoVatContainer1 db = new RaoVatContainer1();

        //
        // GET: /NewsColumn/

        public ActionResult Index()
        {
            return View(db.NewsColumns.ToList());
        }

        //
        // GET: /NewsColumn/Details/5

        public ActionResult Details(int id = 0)
        {
            NewsColumn newscolumn = db.NewsColumns.Find(id);
            if (newscolumn == null)
            {
                return HttpNotFound();
            }
            return View(newscolumn);
        }

        //
        // GET: /NewsColumn/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /NewsColumn/Create

        [HttpPost]
        public ActionResult Create(NewsColumn newscolumn)
        {
            if (ModelState.IsValid)
            {
                db.NewsColumns.Add(newscolumn);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(newscolumn);
        }

        //
        // GET: /NewsColumn/Edit/5

        public ActionResult Edit(int id = 0)
        {
            NewsColumn newscolumn = db.NewsColumns.Find(id);
            if (newscolumn == null)
            {
                return HttpNotFound();
            }
            return View(newscolumn);
        }

        //
        // POST: /NewsColumn/Edit/5

        [HttpPost]
        public ActionResult Edit(NewsColumn newscolumn)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newscolumn).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(newscolumn);
        }

        //
        // GET: /NewsColumn/Delete/5

        public ActionResult Delete(int id = 0)
        {
            NewsColumn newscolumn = db.NewsColumns.Find(id);
            if (newscolumn == null)
            {
                return HttpNotFound();
            }
            return View(newscolumn);
        }

        //
        // POST: /NewsColumn/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsColumn newscolumn = db.NewsColumns.Find(id);
            db.NewsColumns.Remove(newscolumn);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}