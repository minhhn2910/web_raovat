﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
namespace RaoVat.Controllers
{
   
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
  
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /*
              [HttpGet]
              public ActionResult Login()
              {
                  return View();
              }
         * 
            [HttpPost]
              public ActionResult Login(Models.LoginModel user)
              {
                  if (ModelState.IsValid)
                  {
                      if (user.IsValid(user.UserName, user.Password))
                      {
                          FormsAuthentication.SetAuthCookie(user.UserName, user.RememberMe);
                          return RedirectToAction("Index", "Home");
                      }
                      else
                      {
                          ModelState.AddModelError("", "Login data is incorrect!");
                      }
                  }
                  return View(user);
              }
         * */
    }
}
