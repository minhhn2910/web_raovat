﻿SET IDENTITY_INSERT Zone ON;

MERGE INTO Zone AS Target 
USING (VALUES 
        (1,N'Hòa Thành'),
		(2,N'Thành phố Tây Ninh'),
		(3,N'Dương Minh Châu'),
		(4,N'Châu Thành'),
		(5,N'Gò Dầu'),
		(6,N'Bến Cầu'),
		(7,N'Tân Châu'),
		(8,N'Tân Biên'),
		(9,N'Trảng Bàng')

) 
AS Source (ZoneID, ZoneName) 
ON Target.ZoneID = Source.ZoneID 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (ZoneID, ZoneName) 
VALUES (ZoneID, ZoneName);

SET IDENTITY_INSERT Zone OFF;

SET IDENTITY_INSERT UserProfile ON;

MERGE INTO UserProfile AS Target 
USING (VALUES 
        (1,'admin')
) 
AS Source (UserId, UserName) 
ON Target.UserId = Source.UserId 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (UserId, UserName) 
VALUES (UserId, UserName);

SET IDENTITY_INSERT UserProfile OFF;

SET IDENTITY_INSERT PostCategory ON;

MERGE INTO PostCategory AS Target 
USING (VALUES 
        (1, N'Điện thoại', N'dienthoai',1), 
        (2, N'Máy Tính & Điện Tử', N'maytinhdientu',1), 
        (3, N'Ôtô & Xe Máy', N'otoxemay',1), 
		(4, N'Nhà Đất', N'nhadat',1), 
		(5,N'Đào Tạo & Việc Làm',N'daotaovieclam',1), 
		(6,N'Thời Trang & Mỹ Phẩm',N'thoitrangmypham',1), 
		(7,N'Khách Sạn & Ẩm Thực',N'khachsanamthuc',1), 
		(8,N'Dịch Vụ Du Lịch',N'dichvudulich',1), 
		(9,N'Dịch Vụ Giải Trí',N'dichvugiaitri',1), 
		(10,N'Dịch Vụ Tài Chính',N'dichvutaichinh',1), 
		(11,N'Dịch Vụ Sửa Chữa',N'dichvusuachua',1), 
		(12,N'Rao Vặt Linh Tinh',N'raovatlinhtinh',1), 
		(13,N'Tổng Hợp',N'tonghop',1), 
		(14,N'Thông Báo',N'thongbao',1)
) 
AS Source (CategoryId, CategoryName, CategoryAlias, ModUserID) 
ON Target.CategoryId = Source.CategoryId 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (CategoryId, CategoryName, CategoryAlias, ModUserID) 
VALUES (CategoryId, CategoryName, CategoryAlias, ModUserID);

SET IDENTITY_INSERT PostCategory OFF;
SET IDENTITY_INSERT Post ON;

MERGE INTO Post AS Target 
USING (VALUES 
        (1, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									1,
									150,
									'photo.jpg'
									), 

        (2, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									1,
									150,
									'photo.jpg'
									), 

									        (3, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									), 
									        (4, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									), 
			(5, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									),
		(6, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									),
	(7, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									),
	(8, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									),
		(9, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',4, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									),
	(10, 1,N'Cần thuê chung cư TDH Trường Thọ Thủ Đức giá rẻ','can-thue-chung-cu-tdh-truong-tho-thu-duc-gia-re',1, N'Tôi cần mua 1 trong 2 căn hộ số 4 hoặc số 5 của tòa A2 chung cư Thăng Long
										Garden - 250 Minh Khai và 1 căn 3PN cũng cùng chung cư 250 Minh Khai...',

									N'<p style="font-size:10pt; font-weight:bold; margin:0px; line-height:45px;">PIN SẠC DỰ PHÒNG YOOKRX 6600 MAH – NGUỒN NĂNG LƯỢNG DỒI DÀO CHO CÁC THIẾT BỊ ĐIỆN TỬ.</p>
									<strong>Thông tin sản phẩm:</strong>
									<p>	- Tên sản phẩm: Pin sạc dự phòng Yookrx 6600mAh.</p>
									<p>	- Màu sắc: Đen, trắng.</p>
									<p>	- Kích thước: 6x12x1cm</p>
									<p>	- Dung lượng: 6600 mAh</p>
									<p>	- Điều kiện bảo hành:</p>
									<p>	+ Đổi trả trong vòng 7 ngày, kể từ ngày KH nhận sản phẩm.</p>
									<p>	+ Bảo hành 06 tháng. KH nhận lại sản phẩm, sau 7 ngày gửi sản phẩm bảo hành tại Hotdeal.</p>',
									'289.0000đ',
									2,
									1,
									0,
									150,
									'photo.jpg'
									)

) 
AS Source (PostId, UserID, Title,TitleAlias,CategoryID,ShortDescription,Detail,Price,ZoneID,Active,PostType,HitCount,Thumbnail) 
ON Target.PostId = Source.PostId 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (PostId, UserID, Title,TitleAlias,CategoryID,ShortDescription,Detail,Price,ZoneID,Active,PostType,HitCount,Thumbnail) 
VALUES (PostId, UserID, Title,TitleAlias,CategoryID,ShortDescription,Detail,Price,ZoneID,Active,PostType,HitCount,Thumbnail);

SET IDENTITY_INSERT Post OFF;
SET IDENTITY_INSERT PostImage ON;

MERGE INTO PostImage AS Target 
USING (VALUES 
        (1, 1,N'product-detail-1.jpg'), 
		(2, 1,N'product-detail-2.jpg'), 
		(3, 1,N'product-detail-3.jpg'), 
		(4, 1,N'product-detail-1.jpg'), 
		(5, 2,N'product-detail-3.jpg'), 
		(6, 2,N'product-detail-2.jpg'), 
		(7, 2,N'product-detail-1.jpg'), 
		(8, 3,N'product-detail-1.jpg'), 
		(9, 3,N'product-detail-3.jpg'), 
		(10, 4,N'product-detail-1.jpg'), 
		(11, 5,N'product-detail-1.jpg'), 
		(12, 6,N'product-detail-3.jpg'), 
		(13, 6,N'product-detail-2.jpg')

) 
AS Source (ImageId, PostID, ImageURL) 
ON Target.ImageId = Source.ImageId 
WHEN NOT MATCHED BY TARGET THEN 
INSERT (ImageId, PostID, ImageURL) 
VALUES (ImageId, PostID, ImageURL);

SET IDENTITY_INSERT PostImage OFF;
GO
