﻿CREATE TABLE [dbo].[PostCategory]
(
	[CategoryId] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY, 
    [CategoryName] NVARCHAR(500) NULL, 
    [CategoryAlias] NVARCHAR(50) NULL, 
    [ModUserId] INT NOT NULL, 
    [CustomAds] NVARCHAR(500) NULL, 
    CONSTRAINT [FK_Category_User] FOREIGN KEY ([ModUserId]) REFERENCES [UserProfile]([UserId])
)
